package wi24rd.radar;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.springframework.util.ResourceUtils;
import org.springframework.util.StreamUtils;

import com.alipay.remoting.exception.RemotingException;
import com.google.common.collect.Lists;
import com.timevary.radar.tcp.client.RadarTcpClient;

import com.timevary.radar.tcp.component.RequestAddressUtil;
import com.timevary.radar.tcp.component.RequestRadarUtil;
import com.timevary.radar.tcp.config.RadarTcpClientProperties;
import com.timevary.radar.tcp.domain.dto.RadarUpgradeFirmwareDto;
import com.timevary.radar.tcp.protocol.RadarProtocolManager;
import com.timevary.radar.tcp.protocol.RadarProtocolSwitch;

public class Client {

    static RadarTcpClient radarTcpClient;

    public static void main(String[] args) {
        System.setProperty("logging.path", "./logs/client");
        System.setProperty("com.alipay.remoting.client.log.level", "info");
        RadarTcpClientProperties radarTcpClientProperties = new RadarTcpClientProperties();
        radarTcpClientProperties.setInvokeTimeout(10000);
        radarTcpClientProperties.setHost("127.0.0.1");
        radarTcpClientProperties.setPort(8802);
        radarTcpClientProperties.setDumpHex(true);
        RadarProtocolManager.turnOnSwitch(RadarProtocolSwitch.DUMP_HEX);
        radarTcpClient = new RadarTcpClient(null, radarTcpClientProperties);
        radarTcpClient.startup();
        new RequestRadarUtil(radarTcpClient);
        new RequestAddressUtil(radarTcpClient);
        System.out.println("hello client");
        try {

            try {
                Collection<String> a = RequestAddressUtil.getRadarListDto().getAddressMap().values();
                for (String radarID : a) {
                    System.out.println(radarID);
                    System.out.println("HeatMap " + RequestRadarUtil.isHeatMapEnable(radarID));
                    try {
                        testUpgradeFirmware(radarID);
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
                // System.out.println("Connected Radar id "+RequestAddressUtil.getRadarListDto());
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } catch (RemotingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        radarTcpClient.shutdown();
    }

    public static void testUpgradeFirmware(String radarId) throws RemotingException, IOException {
        File file = ResourceUtils.getFile("/home/w/Documents/fall.1.7.4.46.bin");
        // Assertions.assertNotNull(file, "firmware file is empty");
        byte[] data = StreamUtils.copyToByteArray(new FileInputStream(file));
        RadarUpgradeFirmwareDto radarUpgradeFirmwareDto = new RadarUpgradeFirmwareDto();
        radarUpgradeFirmwareDto.setRadarIds(Lists.newArrayList(radarId));
        radarUpgradeFirmwareDto.setFirmwareData(data);
        radarUpgradeFirmwareDto.setToVersion("01.07.04.46");
        List<String> upgraded = RequestRadarUtil.upgradeFirmware(radarUpgradeFirmwareDto);
        System.out.println("upgraded firmware radar ids: " + upgraded);
        if (upgraded.size() == 1) {
            System.out.println("Upgrade Sucess");
        }
    }

}
