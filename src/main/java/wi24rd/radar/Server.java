package wi24rd.radar;

import com.google.common.collect.Sets;
import com.timevary.radar.tcp.component.ConnectionCallUtil;
import com.timevary.radar.tcp.component.ServerRequestRadarUtil;
import com.timevary.radar.tcp.config.RadarTcpServerProperties;
import com.timevary.radar.tcp.connection.RadarAddressHashMap;
import com.timevary.radar.tcp.connection.RadarAddressMap;
import com.timevary.radar.tcp.hander.RadarProtocolDataHandler;
import com.timevary.radar.tcp.protocol.*;
import com.timevary.radar.tcp.server.RadarTcpServer;
import com.timevary.radar.tcp.util.ByteUtil;
import java.util.HashSet;
import java.util.Set;

public class Server {

    public static void main(String[] args) {
        System.setProperty("logging.path", "./logs/server");
        String serverAddress = "localhost:8802";
        RadarAddressMap radarAddressMap = new RadarAddressHashMap(serverAddress);
        RadarTcpServerProperties properties = new RadarTcpServerProperties();
        properties.setPrintOnlineRadar(true);
        properties.setDumpHex(true);
        properties.setHost("0.0.0.0");
        properties.setPort(8802);
        // properties.setLogFirmwareUpgrade(true);
        properties.setOpenServerRoute(true);
        RadarTcpServer radarTcpServer = new RadarTcpServer(radarAddressMap, properties);
        RadarProtocolManager.turnOnSwitch(RadarProtocolSwitch.DUMP_HEX);
        radarTcpServer.registerHandler(new RadarProtocolDataHandler() {
            @Override
            public Object process(RadarProtocolData protocolData) {
                System.out.println(protocolData.getId());
                protocolData.setData(ByteUtil.intToByteBig(1));
                return protocolData;
            }

            @Override
            public Set<FunctionEnum> interests() {
                HashSet<FunctionEnum> functionEnums = Sets.newHashSet(FunctionEnum.values());
                // To prevent duplicate registration. Don't delete. They are handled after these 'remove'.
                functionEnums.remove(FunctionEnum.NegFallDetect);
                functionEnums.remove(FunctionEnum.AT_MODE);
                functionEnums.remove(FunctionEnum.FallAsleep);
                functionEnums.remove(FunctionEnum.awake);
                return functionEnums;
            }
        });
        ConnectionCallUtil connectionCallUtil = new ConnectionCallUtil(radarTcpServer);
        ServerRequestRadarUtil serverRequestRadarUtil = new ServerRequestRadarUtil(radarTcpServer);
        radarTcpServer.startup();
    }
}
